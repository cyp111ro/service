<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'booking_id' => $this->id,
            'mechanic_name' => $this->mechanic->name,
            'numberplate' => $this->numberplate,
            'service_type' => $this->serviceType->name,
            'from_date' => $this->from_date,
            'to_date' => $this->to_date,
        ];
    }
}
