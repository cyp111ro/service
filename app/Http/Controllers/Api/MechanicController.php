<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MechanicRequest;
use App\Http\Resources\MechanicResource;
use App\Models\Mechanic;

class MechanicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mechanics = Mechanic::all();
        return response([
            MechanicResource::collection($mechanics),
        ], 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MechanicRequest $request)
    {
        $data = $request->all();
        $mechanic = Mechanic::create($data);

        return response()->json([
            'mechanic' => new MechanicResource($mechanic),
            'message' => 'Stored successfully'
        ], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Mechanic $mechanic
     * @return \Illuminate\Http\Response
     */
    public function show(Mechanic $mechanic)
    {
        return response([
            'mechanic' => new MechanicResource($mechanic),
            'message' => 'Retrieved successfully'
        ]);
    }


    /**
     * @param MechanicRequest $request
     * @param Mechanic $mechanic
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(MechanicRequest $request, Mechanic $mechanic)
    {
        $mechanic->update($request->all());

        return response()->json([
            'mechanic' => new MechanicResource($mechanic),
            'message' => "{$mechanic->name()} updated successfully"
        ]);
    }

    /**
     * @param Mechanic $mechanic
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy(Mechanic $mechanic)
    {
        $mechanic->delete();
        return response()->json([
            'message' => "Mechanic deleted successfully"
        ]);
    }
}
