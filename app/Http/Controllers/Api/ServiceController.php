<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceRequest;
use App\Http\Resources\ServiceResource;
use App\Models\Service;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Services = Service::with('serviceType')->get();
        return response([
            ServiceResource::collection($Services),
        ], 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ServiceRequest $request)
    {
        $data = $request->all();
        $Service = Service::create($data);

        return response()->json([
            'Service' => new ServiceResource($Service),
            'message' => 'Stored successfully'
        ], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Service $Service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $Service)
    {
        return response([
            'Service' => new ServiceResource($Service),
            'message' => 'Retrieved successfully'
        ]);
    }


    /**
     * @param ServiceRequest $ServiceRequest
     * @param Service $Service
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ServiceRequest $ServiceRequest, Service $Service)
    {
        $Service->update($ServiceRequest->all());

        return response()->json([
            'Service' => new ServiceResource($Service),
            'message' => "{$Service->name()} updated successfully"
        ]);
    }

    /**
     * @param Service $Service
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy(Service $Service)
    {
        $Service->delete();
        return response()->json([
            'message' => "Service deleted successfully"
        ]);
    }
}
