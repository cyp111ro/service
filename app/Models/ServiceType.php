<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    /**
     * Get the services for the service type.
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'service_types_services');
    }

    /**
     * Get the mechanic that owns the booking.
     */
    public function booking()
    {
        return $this->belongsToMany(Booking::class);
    }

}
