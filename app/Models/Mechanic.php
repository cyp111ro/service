<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mechanic extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    /**
     * Bookings
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bookings()
    {
        return $this->belongsTo(Booking::class);
    }

}
