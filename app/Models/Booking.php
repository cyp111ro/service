<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = ['mechanic_id', 'from_date', 'to_date', 'numberplate', 'service_type_id', 'status'];

    protected $with = ['mechanic', 'serviceType'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mechanic()
    {
        return $this->hasOne(Mechanic::class, 'id', 'mechanic_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function serviceType()
    {
        return $this->hasOne(ServiceType::class, 'id', 'service_type_id');
    }

    /**
     * Returns all bookings from a selected day, defaults to the current day
     * If mechanic_id is in the request will also filter
     *
     * @param $query
     * @return mixed
     */
    public function scopeFilter($query)
    {
        $from_date = Carbon::now();
        $to_date = Carbon::now();
        if (request()->has('date')) {
            $from_date = Carbon::createFromFormat('d/m/Y', request()->get('date'));
            $to_date = Carbon::createFromFormat('d/m/Y', request()->get('date'));
        }
        $from_date->setTime(0, 0, 0);
        $to_date->setTime(23, 59, 59);

        if (request()->has('mechanic_id')) {
            return $query->where('mechanic_id', request()->get('mechanic_id'))->where('from_date', '>=', $from_date)->where('from_date', '<', $to_date);
        }

        return $query->where('from_date', '>=', $from_date)->where('from_date', '<', $to_date);
    }

}
