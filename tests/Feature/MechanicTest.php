<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class MechanicSanctumTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_mechanics_logged_in()
    {
        $this->seed();
        Sanctum::actingAs(
            User::first()
        );
        $response = $this->get('/api/mechanic');
        $response->assertOk();
    }

    public function test_get_mechanics_not_logged_in()
    {
        $this->seed();
        $response = $this->get('/api/mechanic');
        $response->assertStatus(302);
    }

    public function test_able_to_insert_mechanic_logged_in()
    {
        $this->seed();
        Sanctum::actingAs(
            User::first()
        );
        $response = $this->post('/api/mechanic', ['name' => 'James']);
        $response->assertStatus(201);
    }

    public function test_unable_to_insert_mechanic_not_logged_in()
    {
        $this->seed();
        $response = $this->post('/api/mechanic', ['name' => 'James']);
        $response->assertStatus(302);
    }

    public function test_mechaninc_fails_when_too_long_logged_in()
    {
        $this->seed();
        Sanctum::actingAs(
            User::first()
        );
        $response = $this->post('/api/mechanic', ['name' => 'thisIsAVeryLongTextSoItFails']);
        $response->assertJsonFragment(['The name is too long']);
    }

    public function test_mechaninc_fails_when_name_or_less_than_minimum_logged_in()
    {
        $this->seed();
        Sanctum::actingAs(
            User::first()
        );
        $response = $this->post('/api/mechanic', ['name' => 'th']);
        $response->assertJsonFragment(['The name is too short']);
        #Tests can be cumulated if similar
        $response = $this->post('/api/mechanic', ['name' => '']);
        $response->assertJsonFragment(['A name is required']);
    }

}
