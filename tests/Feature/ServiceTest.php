<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ServiceTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_services_logged_in()
    {
        $this->seed();
        $response = $this->get('/api/service');
        $response->assertOk();
    }

    public function test_services_available_not_logged_in()
    {
        $this->seed();
        $response = $this->get('/api/service');
        $response->assertStatus(200);
    }

    public function test_able_to_insert_service_logged_in()
    {
        $this->seed();
        Sanctum::actingAs(
            User::first()
        );
        $response = $this->post('/api/service', ['name' => 'James', 'price' => 21]);
        $response->assertStatus(201);
    }

    public function test_unable_to_insert_service_not_logged_in()
    {
        $this->seed();
        $response = $this->post('/api/service', ['name' => 'James', 'price' => 21]);
        $response->assertStatus(302);
    }

    public function test_service_fails_when_too_long_logged_in()
    {
        $this->seed();
        Sanctum::actingAs(
            User::first()
        );
        $response = $this->post('/api/service', ['name' => 'thisIsALongStringXASDASD', 'price' => 21]);
        $response->assertJsonFragment(['The name is too long']);
    }

    public function test_service_fails_when_name_or_less_than_minimum_logged_in()
    {
        $this->seed();
        Sanctum::actingAs(
            User::first()
        );
        $response = $this->post('/api/service', ['name' => 'th', 'price' => 21]);
        $response->assertJsonFragment(['The name is too short']);
        #Tests can be cumulated if similar
        $response = $this->post('/api/service', ['name' => '', 'price' => 21]);
        $response->assertJsonFragment(['A name is required']);
    }

}
