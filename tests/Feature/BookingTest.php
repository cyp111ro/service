<?php

namespace Tests\Feature;

use App\Models\Mechanic;
use App\Models\ServiceType;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class BookingTest extends TestCase
{

    use RefreshDatabase;


    public function test_view_bookings_not_logged_in()
    {
        $this->seed();
        $response = $this->get('/api/booking');
        $response->assertOk();
    }

    /**
     * Weird fail needs investigated or find a different solution
     */
    public function test_view_bookings_certain_day_not_logged_in()
    {
        $this->seed();
        $response = $this->post('/api/booking/filter', ['date' => Carbon::now()->format('d/m/Y')]);
        $response->assertJsonFragment([Carbon::now()->format('Y-m-d')]);
    }

    public function test_view_bookings_certain_day_user_not_logged_in()
    {
        $this->seed();
        $mechanic = Mechanic::inRandomOrder()->first();
        $response = $this->post('/api/booking/filter', ['date' => Carbon::now()->format('d/m/Y'), 'mechanic_id' => $mechanic->id]);
        $response->assertJsonFragment([$mechanic->name]);
    }


    public function test_if_user_can_add_booking_logged_in()
    {
        $this->seed();
        Sanctum::actingAs(
            User::first()
        );
        $response = $this->post('/api/booking', [
            'from_date' => now(),
            'to_date' => now()->addMinutes(30),
            'numberplate' => 'YL14DLX',
            'mechanic_id' => Mechanic::inRandomOrder()->first()->id,
            'service_type_id' => ServiceType::inRandomOrder()->first()->id
        ]);
        $response->assertStatus(201);
    }
}
