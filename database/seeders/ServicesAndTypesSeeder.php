<?php

namespace Database\Seeders;

use App\Models\Service;
use App\Models\ServiceType;
use Illuminate\Database\Seeder;

class ServicesAndTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $serviceTypesArray = [
            [
                'name' => 'Full Service',
                'services' => [
                    'Oil Change',
                    'Oil Filter Change',
                    'Diesel Filter Change'
                ]
            ],
            [
                'name' => 'Interim Service',
                'services' => [
                    'Tyres Change',
                    'Oil Filter Change',
                    'Diesel Filter Change',
                    'Break Fluid Change'
                ]

            ]
        ];

        foreach ($serviceTypesArray as $key => $serviceTypeArray) {
            $serviceType = ServiceType::firstOrCreate(['name' => $serviceTypeArray['name']]);
            foreach ($serviceTypeArray['services'] as $serviceName) {
                $service = Service::firstOrCreate(['name' => $serviceName], ['price' => rand(34, 150)]);
                $service->serviceType()->attach($serviceType);
            }
        }
    }
}
