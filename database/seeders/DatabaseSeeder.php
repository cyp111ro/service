<?php

namespace Database\Seeders;

use App\Models\Mechanic;
use App\Models\Service;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Mechanic::factory(10)->create();
        $this->call([
            UserSeeder::class,
            ServicesAndTypesSeeder::class,
            BookingsSeeder::class
        ]);
    }
}
