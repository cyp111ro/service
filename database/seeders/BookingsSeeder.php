<?php

namespace Database\Seeders;

use App\Models\Booking;
use App\Models\Mechanic;
use App\Models\ServiceType;
use Carbon\CarbonPeriod;
use Illuminate\Container\Container;
use Illuminate\Database\Seeder;
use Faker\Generator;

class BookingsSeeder extends Seeder
{
    /**
     * The current Faker instance.
     *
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * Create a new seeder instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->faker = $this->withFaker();
    }

    /**
     * Get a new Faker instance.
     *
     * @return \Faker\Generator
     */
    protected function withFaker()
    {
        return Container::getInstance()->make(Generator::class);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $period = CarbonPeriod::create(now()->subDays(2), now()->addDays(5));

        // Convert the period to an array of dates
        $carbonDates = $period->toArray();

        //Create bookings
        $mechanics = Mechanic::all();
        $mechanics->each(function (Mechanic $mechanic) use ($carbonDates) {
            foreach ($carbonDates as $carbonDate) {
                $statuses = ['pending', 'paid', 'completed'];
                $statusIndex = array_rand($statuses);
                Booking::create([
                    'mechanic_id' => $mechanic->id,
                    'from_date' => $carbonDate,
                    'to_date' => $carbonDate->addMinutes(30),
                    'numberplate' => $this->faker->numerify('DC##DSL'),
                    'service_type_id' => ServiceType::inRandomOrder()->first()->id,
                ]);
            }
        });

    }
}
