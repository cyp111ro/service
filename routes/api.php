<?php


use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BookingController;
use App\Http\Controllers\Api\MechanicController;
use App\Http\Controllers\Api\ServiceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::get('/service', [ServiceController::class, 'index']);
Route::get('/booking', [BookingController::class, 'index']);
Route::any('/booking/filter', [BookingController::class, 'filter']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::apiResource('mechanic', MechanicController::class);
    Route::apiResource('service', ServiceController::class, ['except' => ['index']]);
    Route::apiResource('booking', BookingController::class, ['except' => ['index']]);
});

